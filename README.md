### Running application
```sh
$ mvn spring-boot:run
```

### Running automated tests
```sh
$ mvn test
```

### Calls

a) Get all products excluding relationships
```sh
$ curl http://localhost:8080/rest/avenuecode/products/short
```

b) Get all products including specified relationships
```sh
$ curl http://localhost:8080/rest/avenuecode/products/full
```

c) Same as 1 using specific product identity
```sh
$ curl http://localhost:8080/rest/avenuecode/products/1/short
```

d) Same as 2 using specific product identity
```sh
$ curl http://localhost:8080/rest/avenuecode/products/1/full
```

e) Get set of child products for specific product
```sh
$ curl http://localhost:8080/rest/avenuecode/products/3/productsset
```

f) Get set of images for specific product
```sh
$ curl http://localhost:8080/rest/avenuecode/products/1/imagesset
```