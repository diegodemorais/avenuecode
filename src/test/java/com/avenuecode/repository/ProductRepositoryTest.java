package com.avenuecode.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.model.*;
import java.util.*;
import static org.junit.Assert.fail;

/**
 *
 * @author DM
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRepositoryTest{
   
    @Autowired
    private ProductRepository productRepository;
    
    @Test
    public void populate(){
        try {
            Product product1 = new Product("Camiseta","Camiseta Rip Curl Especial");
            productRepository.save(product1);

            Image image1 = new Image("Frente",product1);
            Image image2 = new Image("Costa",product1);
            Image image3 = new Image("Perfil",product1);
            List<Image> imagesProduct1 = new ArrayList<Image>(){{
                add(image1);
                add(image2);
                add(image3);
            }};
    //        imageRepository.save(imagesProduct1);        
            product1.setImages(imagesProduct1);
            productRepository.save(product1);       


            Product product2 = new Product("Bermuda","Bermuda Onbongo Tecido");
            productRepository.save(product2);

            Image image4 = new Image("Frente",product2);
            Image image5 = new Image("Perfil",product2);
            List imagesProduct2 = new ArrayList<Image>(){{
                add(image4);
                add(image5);
            }};
    //        imageRepository.save(imagesProduct2);        

            product2.setImages(imagesProduct2);
            productRepository.save(product2);  

            Product product3 = new Product("Blusa","Blusa Billabong Capuz");
            productRepository.save(product3);

            product1.setParent(product3);
            productRepository.save(product1);

            Image image6 = new Image("Frente",product3);
            List imagesProduct3 = new ArrayList<Image>(){{
                add(image6);
            }};
    //        imageRepository.save(imagesProduct2);        
            product3.setImages(imagesProduct3);
            List children1 = new ArrayList<Product>(){{
                add(product1);
            }};
            product3.setChildren(children1);        
            productRepository.save(product3);  

    //        String result = "";
    //        for (Product product : productRepository.findAll()) {
    //            result += (product.toString());
    //        }
        } catch (Exception e){
            fail("Falha: " + e);
        }
        
        
    }
    
   
   
}