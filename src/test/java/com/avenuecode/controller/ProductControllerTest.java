package com.avenuecode.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 *
 * @author DM
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest{
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void callA() throws Exception {
        this.mockMvc.perform(get("/products/short").accept(MediaType.parseMediaType("application/json")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("Product[id=1, name='Camiseta', description='Camiseta Rip Curl Especial']\r\n" +
                                            "Product[id=2, name='Bermuda', description='Bermuda Onbongo Tecido']\r\n" +
                                            "Product[id=3, name='Blusa', description='Blusa Billabong Capuz']\r\n"));
    }
    
    @Test
    public void callB() throws Exception {
        this.mockMvc.perform(get("/products/full").accept(MediaType.parseMediaType("application/json")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("Product[id=1, name='Camiseta', description='Camiseta Rip Curl Especial']\r\n" +
                                            "Image[id=1, type='Frente']\r\n" +
                                            "Image[id=2, type='Costa']\r\n" +                        
                                            "Image[id=3, type='Perfil']\r\n" +                        
                                            "Product[id=2, name='Bermuda', description='Bermuda Onbongo Tecido']\r\n" +
                                            "Image[id=4, type='Frente']\r\n" +
                                            "Image[id=5, type='Perfil']\r\n" +
                                            "Product[id=3, name='Blusa', description='Blusa Billabong Capuz']\r\n" +
                                            "Product child[id=1, name='Camiseta', description='Camiseta Rip Curl Especial']\r\n" +
                                            "Image[id=6, type='Frente']\r\n"));
    }    
    
    @Test
    public void callC() throws Exception {
        this.mockMvc.perform(get("/products/1/short").accept(MediaType.parseMediaType("application/json")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("Product[id=1, name='Camiseta', description='Camiseta Rip Curl Especial']\r\n"));
    }   
    
    @Test
    public void callD() throws Exception {
        this.mockMvc.perform(get("/products/1/full").accept(MediaType.parseMediaType("application/json")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("Product[id=1, name='Camiseta', description='Camiseta Rip Curl Especial']\r\n" +
                                            "Image[id=1, type='Frente']\r\n" +
                                            "Image[id=2, type='Costa']\r\n" +
                                            "Image[id=3, type='Perfil']\r\n"));
    }     
    
    @Test
    public void callE() throws Exception {
        this.mockMvc.perform(get("/products/3/productsset").accept(MediaType.parseMediaType("application/json")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("Product child[id=1, name='Camiseta', description='Camiseta Rip Curl Especial']\r\n"));
    } 

    @Test
    public void callF() throws Exception {
        this.mockMvc.perform(get("/products/1/imagesset").accept(MediaType.parseMediaType("application/json")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("Image[id=1, type='Frente']\r\n" +
                                            "Image[id=2, type='Costa']\r\n" +
                                            "Image[id=3, type='Perfil']\r\n"));
    }      
   
}