package com.avenuecode;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;

/**
 *
 * @author DM
 */

@SpringBootApplication
public class Application implements CommandLineRunner {
//    private static final Logger logger = LoggerFactory.getLogger(Application.class);

//    @Autowired
//    private ProductRepository productRepository;

////    @Autowired
////    private ImageRepository imageRepository;
        
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    @Transactional
    public void run(String... strings) throws Exception {
//        
//        //Populating and testing repository
//        final Product product1 = new Product("Camiseta","Camiseta Rip Curl Especial");
//        productRepository.save(product1);
//        
//        final Image image1 = new Image("Frente",product1);
//        final Image image2 = new Image("Costa",product1);
//        final Image image3 = new Image("Perfil",product1);
//        Set imagesProduct1 = new HashSet<Image>(){{
//            add(image1);
//            add(image2);
//            add(image3);
//        }};
////        imageRepository.save(imagesProduct1);        
//        product1.setImages(imagesProduct1);
//        productRepository.save(product1);       
//        
//        
//        Product product2 = new Product("Bermuda","Bermuda Onbongo Tecido");
//        productRepository.save(product2);
//        
//        final Image image4 = new Image("Frente",product2);
//        final Image image5 = new Image("Perfil",product2);
//        Set imagesProduct2 = new HashSet<Image>(){{
//            add(image4);
//            add(image5);
//        }};
////        imageRepository.save(imagesProduct2);        
//        
//        product2.setImages(imagesProduct2);
//        productRepository.save(product2);  
//
//        final Product product3 = new Product("Blusa","Blusa Billabong Capuz");
//        productRepository.save(product3);
//        
//        product1.setParent(product3);
//        productRepository.save(product1);
//        
//        final Image image6 = new Image("Frente",product3);
//        Set imagesProduct3 = new HashSet<Image>(){{
//            add(image6);
//        }};
////        imageRepository.save(imagesProduct2);        
//        product3.setImages(imagesProduct3);
//        Set children1 = new HashSet<Product>(){{
//            add(product1);
//        }};
//        product3.setChildren(children1);        
//        productRepository.save(product3);  
//
//        for (Product product : productRepository.findAll()) {
//            logger.info(product.toString());
//        }
    }
 }