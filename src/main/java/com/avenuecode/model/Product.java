package com.avenuecode.model;

import javax.persistence.*;
import java.util.List;

/**
 *
 * @author DM
 */

@Entity
@Table(name = "product")
public class Product { 
  
    private int id;
    
    private String name;
    private String description;
    
//    @Column (name = "parent_product_id")    
    private Product parent;
    private int parent_product_id;
    private List<Product> children;
    
    private List<Image> images;
    
    public Product(){

    }

    public Product(String name, String description) {
        setName(name);
        setDescription(description);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)    
    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
    
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)    
    public List<Product> getChildren() {
        return children;
    }

    public void setChildren(List<Product> products) {
        this.children = products;
    }
    
    @ManyToOne
    @JoinColumn(name = "parent_product_id")
    public Product getParent() {
        return parent;
    }

    public void setParent(Product product) {
        this.parent = product;
    }


    public String toStringFull() {
        String result = toStringShort() + toStringChilds() + toStringImages();
        return result;
    }
 
    public String toStringShort() {
        String result = String.format("Product[id=%d, name='%s', description='%s']%n",
                id, name, description);
        return result;
    }  
//    
    public String toStringChilds(){
        String result = "";
        if (children != null) {
            for(Product prod : children) {
                result += String.format(
                    "Product child[id=%d, name='%s', description='%s']%n",
                    prod.getId(), prod.getName(), prod.getDescription());
            }
        }
        return result;
    }

    public String toStringImages(){
        String result = "";
        if (images != null) {
            for(Image image : images) {
                result += String.format(
                    "Image[id=%d, type='%s']%n",
                    image.getId(), image.getType());
            }
        }
        return result;
    }    
}