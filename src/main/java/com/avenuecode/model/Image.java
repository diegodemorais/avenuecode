package com.avenuecode.model;

import javax.persistence.*;

/**
 *
 * @author DM
 */

@Entity
@Table(name = "image")
public class Image {
    private int id;
    private String type;
    private Product product;
    private int product_id;

    public Image() {

    }

    public Image(String type) {
        setType(type);
    }

    public Image(String type, Product product) {
        setType(type);
        setProduct(product);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @ManyToOne
    @JoinColumn(name = "product_id")
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}