package com.avenuecode.repository;

import com.avenuecode.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author DM
 */

@RepositoryRestResource
public interface ProductRepository extends CrudRepository<Product, Integer> {
    
}


