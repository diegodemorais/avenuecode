package com.avenuecode.service;

import com.avenuecode.model.*;
import com.avenuecode.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 *
 * @author DM
 */
@Component
public class ProductService {
    
    @Autowired
    ProductRepository productRepository;
    
    public ProductService(){
        
    }

    public String productsShort(){
        String content = "";
        for (Product product : productRepository.findAll()) {
            content += (product.toStringShort());
        }
        return content;
    }    

    public String productsFull(){
        String content = "";
        for (Product product : productRepository.findAll()) {
            content += (product.toStringFull());
        }
        return content;
    }

    public String productsShortId(int id){
        String content = "";
        Product product = productRepository.findOne(id);
        if (product != null) content = (product.toStringShort());
        return content;
    } 

    public String productsFulltId(int id){
        String content = "";
        Product product = productRepository.findOne(id);
        if (product != null) content = (product.toStringFull());
        return content;
    }   

    public String childrenId(int product_id){
        String content = "";
        Product product = productRepository.findOne(product_id);
        if (product != null) {
            content = (product.toStringChilds());            
            if (content.isEmpty())            
                content = "No children.";
        }
        return content;
    }     
    
    public String imagesId(int product_id){
        String content = "";
        Product product = productRepository.findOne(product_id);
        if (product != null) {
            content = (product.toStringImages());            
            if (content.isEmpty())            
                content = "No images.";
        } 
        return content;
    }     

    public ResponseEntity<String> okRequest(String content) {
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<>(content,responseHeaders,HttpStatus.OK);
    }       

    public ResponseEntity<String> badRequest(){
        String content = "220 Bad Request";
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<>(content,responseHeaders,HttpStatus.BAD_REQUEST);
    }    

}