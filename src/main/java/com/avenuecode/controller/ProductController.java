package com.avenuecode.controller;

import com.avenuecode.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DM
 */
@RestController
public class ProductController {
    @Autowired
    private ProductService productService;
    
    public ProductController(){
        
    }

    //Call a
    @RequestMapping(value = "products/short", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<String> productsShort(){
        String content = productService.productsShort();
        if (content.isEmpty())
            return productService.badRequest();
        else
            return productService.okRequest(content);
    }    
    
    //Call b
    @RequestMapping(value = "products/full", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<String> productsFull(){
        String content = productService.productsFull();
        if (content.isEmpty())
            return productService.badRequest();
        else
            return productService.okRequest(content);
    }

    
    //Call c
    @RequestMapping(value = "/products/{id}/short", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<String> productsShortId(@PathVariable("id") int id){
        String content = productService.productsShortId(id);
        if (content.isEmpty())
            return productService.badRequest();
        else
            return productService.okRequest(content);
    } 
    
    //Call d
    @RequestMapping(value = "/products/{id}/full", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<String> productsFulltId(@PathVariable("id") int id){
        String content = productService.productsFulltId(id);
        if (content.isEmpty())
            return productService.badRequest();
        else
            return productService.okRequest(content);
    }   

    //Call e
    @RequestMapping(value = "products/{product_id}/productsset", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<String> childrenId(@PathVariable("product_id") int product_id){
        String content = productService.childrenId(product_id);
        if (content.isEmpty())
            return productService.badRequest();
        else
            return productService.okRequest(content);
    }     
    
    //Call f
    @RequestMapping(value = "products/{product_id}/imagesset", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<String> imagesId(@PathVariable("product_id") int product_id){
        String content = productService.imagesId(product_id);
        if (content.isEmpty())
            return productService.badRequest();
        else
            return productService.okRequest(content);
    }     

}