INSERT INTO `PRODUCT`(ID, DESCRIPTION, NAME, PARENT_PRODUCT_ID) VALUES
(3, 'Blusa Billabong Capuz', 'Blusa', NULL),
(2, 'Bermuda Onbongo Tecido', 'Bermuda', NULL),
(1, 'Camiseta Rip Curl Especial', 'Camiseta', 3);

INSERT INTO `IMAGE`(ID, TYPE, PRODUCT_ID) VALUES
(1, 'Frente', 1),
(2, 'Costa', 1),
(3, 'Perfil', 1),
(4, 'Frente', 2),
(5, 'Perfil', 2),
(6, 'Frente', 3);

